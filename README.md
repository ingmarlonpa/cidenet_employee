
Bienvenidos al repositorio
Este proyecto es un test, desarrollado en python con su framework para api Django Rest Framework:


Instaladores

1) Compilador

Python3.10


2) IDE

Visual Studio Code
Sublime Text
Pycharm


3) Motor de base de datos

Sqlite Studio


4) Repositorios

Git


Instalación

1) Clonar o descargar el proyecto del repositorio
git clone https://gitlab.com/ingmarlonpa/cidenet_employee.git con https

2) Crear un entorno virtual para posteriormente instalar las librerias del proyecto


python3 -m venv venv (Windows)

python3.10 -m venv nombre_entorno (Linux)

3) Activar el entorno virtual de nuestro proyecto


cd venv\Scripts\activate.bat (Windows)

source venv/bin/active (Linux)


4) Instalar todas las librerias del proyecto 

Ingresa a la carpeta del proyecto y ejecutas el sisguiente comando
pip install -r requirements.txt


5) Crear la base de datos con las migraciones y el superuser para iniciar sesión

Estando dentro de la carpeta del proyecto ejecuta los siguientes comandos para crear la bd:

python manage.py makemigrations
python manage.py migrate

y este comando para crear un superusuario:

python manager createsuperuser


7) Subimos el servidor con el siguiente comando:
  
python manage.py runserver

8) Abrimos el navegador y en la barra de direccionas colocamos el siguiente endpoint

http://localhost:8000/api/employee/

Nota: Se muestra la interfaz web, donde puedes crear, listar, editar y eliminar registros.

para eliminar o editar un registro ejecutamos en el navegador el siguiente endpoint

http://localhost:8000/api/employee/1/

El numero pertenece a cada id que tienen los registros en este caso se editará o eliminara el registro 1
