from rest_framework import serializers
from apps.employee.models import Employee


class EmployeeSerializer(serializers.ModelSerializer):
    """docstring for EmployeeSerializer"""

    class Meta:
        model = Employee
        fields = ('id','first_name', 'second_name', 'surname', 'second_surname', 'country', 'email')
        read_only_fields = ['id', 'email']


