from rest_framework import viewsets, status
from rest_framework.response import Response

from apps.employee.serializers import EmployeeSerializer
from apps.employee.models import Employee


class EmployeeViewSet(viewsets.ModelViewSet):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            email_names = (
                serializer.data['first_name'], serializer.data['surname'].replace(" ", "")
            )
            if serializer.data['country'] == Employee.COLOMBIA:
                email = '.'.join(email_names) + '@cidenet.com.co'
            else:
                email = '.'.join(email_names) + '@cidenet.com.us'

            employee_email = Employee.objects.filter(email=str(email))
            if employee_email.exists():
                contador = 1
                while True:
                    if serializer.data['country'] == Employee.COLOMBIA:
                        email = '.'.join(email_names)+str(contador) + '@cidenet.com.co'
                    else:
                        email = '.'.join(email_names) + str(contador) + '@cidenet.com.us'
                    if Employee.objects.filter(email=str(email)):
                        contador += 1
                    else:
                        email = email
                        created_employee = Employee.objects.create(
                            first_name=serializer.data['first_name'].upper(),
                            second_name=serializer.data['second_name'].upper(),
                            surname=serializer.data['surname'].upper(),
                            second_surname=serializer.data['second_surname'].upper(),
                            country=serializer.data['country'],
                            email=email
                        )
                        created_employee.save()
                        return Response(serializer.data, status=status.HTTP_201_CREATED)

            created_employee = Employee.objects.create(
                first_name=serializer.data['first_name'].upper(),
                second_name=serializer.data['second_name'].upper(),
                surname=serializer.data['surname'].upper(),
                second_surname=serializer.data['second_surname'].upper(),
                country=serializer.data['country'],
                email=email
            )
            created_employee.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        data = request.data
        instance.first_name = data['first_name'].upper()
        instance.second_name = data['second_name'].upper()
        instance.surname = data['surname'].upper()
        instance.second_surname = data['second_surname'].upper()
        instance.save()

        serializer = EmployeeSerializer(instance)
        return Response(serializer.data)