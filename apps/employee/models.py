from django.core.validators import RegexValidator
from django.db import models
from django.utils.translation import gettext_lazy as _


class Employee(models.Model):
    COLOMBIA = 'COLOMBIA'
    USA = 'ESTADOS UNIDOS'
    country_list = [(COLOMBIA, 'COLOMBIA'), (USA, 'ESTADOS UNIDOS')]

    first_name = models.CharField(
        max_length=20,
        db_column="Nombre",
        db_index=True,
        validators=[RegexValidator(
            '^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+$',
            'No puede ingresar numeros, acento, Ñ, ni caracteres especiales',
            'Entrada invalida')]
    )
    surname = models.CharField(
        max_length=20,
        db_index=True,
        db_column="Segundo nombre",
        validators=[RegexValidator(
            '^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+$',
            'No puede ingresar numeros, acento, Ñ, ni caracteres especiales',
            'Entrada invalida')]
    )
    second_name = models.CharField(
        max_length=50,
        blank=True,
        null=True,
        db_column="Apellido",
        validators=[RegexValidator(
            '^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+$',
            'No puede ingresar numeros, acento, Ñ, ni caracteres especiales',
            'Entrada invalida')]
    )

    second_surname = models.CharField(
        max_length=50,
        blank=True,
        null=True,
        db_column='Segundo apellido',
        validators=[RegexValidator(
            '^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+$',
            'No puede ingresar numeros, acento, Ñ, ni caracteres especiales',
            'Entrada invalida')]
    )
    country = models.CharField(
        choices=country_list,
        default=COLOMBIA,
        max_length=20
    )
    email = models.EmailField(
        unique=True,
        help_text=_("Correo electronico"),
        db_column="email",
        max_length=300,
        null=True
    )


    def __str__(self):
        return f'{self.first_name} {self.surname}'


