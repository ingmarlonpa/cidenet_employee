from django.contrib import admin

from apps.employee.models import Employee


@admin.register(Employee)
class EmployeeAdmin(admin.ModelAdmin):
    """docstring for Employee"""

    list_display = (
        "email",
        "first_name",
        "surname",
        "country"
    )
    list_display_links = ("email",)
    search_fields = [
        "email",
        "first_name",
        "surname",
        "country"
    ]

