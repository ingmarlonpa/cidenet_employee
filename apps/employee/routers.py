from rest_framework.routers import DefaultRouter
from apps.employee.views import EmployeeViewSet


router = DefaultRouter(trailing_slash=True)
router.register(r'employee', EmployeeViewSet, basename='employee')